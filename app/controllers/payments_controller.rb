class PaymentsController < ApplicationController
    def new #untuk menampilkan form data baru
        @payment = Payment.new
    end

    def create #untuk memproses data baru yang dimasukan di form new
        payment = Payment.new(resource_params)
        payment.save
        flash[:notice] = 'Payment has been created'
        redirect_to payments_path
    end

    def edit #menampilkan data yang sudah disimpan di edit
        @payment = Payment.find(params[:id])
    end

    def update #melakukan proses ketika user mengedit data
        @payment = Payment.find(params[:id])
        @payment.update(resource_params)
        flash[:notice] = 'Payment has been updated'
        redirect_to payment_path(@payment)
    end

    def destroy #unuk menghapus data
        @payment = Payment.find(params[:id])
        @payment.destroy
        flash[:notice] = 'Payment has been deleted'
        redirect_to payments_path
    end

    def index #menampilkan seluruh data yang ada di database
        @payments = Payment.all
    end

    def show #menampilkan sebuah data secara detail
        id = params[:id]
        @payment = Payment.find(id)
    end

    private
    def resource_params
        params.require(:payment).permit(:transaction_id, :status, :upload)
    end
end
