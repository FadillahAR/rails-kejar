class ExamsController < ApplicationController
    def new #untuk menampilkan form data baru
        @exam = Exam.new
    end

    def create #untuk memproses data baru yang dimasukan di form new
        exam = Exam.new(resource_params)
        exam.save
        flash[:notice] = 'Exam has been created'
        redirect_to exams_path
    end

    def edit #menampilkan data yang sudah disimpan di edit
        @exam = Exam.find(params[:id])
    end

    def update #melakukan proses ketika user mengedit data
        @exam = Exam.find(params[:id])
        @exam.update(resource_params)
        flash[:notice] = 'Exam has been updated'
        redirect_to exam_path(@exam)
    end

    def destroy #unuk menghapus data
        @exam = Exam.find(params[:id])
        @exam.destroy
        flash[:notice] = 'Exam has been deleted'
        redirect_to exams_path
    end

    def index #menampilkan seluruh data yang ada di database
        @exams = Exam.all
    end

    def show #menampilkan sebuah data secara detail
        id = params[:id]
        @exam = Exam.find(id)
    end

    private
    def resource_params
        params.require(:exam).permit(:title, :mapel, :duration, :nilai, :status, :level, :student_id)
    end
end
