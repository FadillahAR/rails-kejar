class StudentsController < ApplicationController
    def new #untuk menampilkan form data baru
        @student = Student.new
    end

    def create #untuk memproses data baru yang dimasukan di form new
        student = Student.new(resource_params)
        student.save
        flash[:notice] = 'Student has been created'
        redirect_to students_path
    end

    def edit #menampilkan data yang sudah disimpan di edit
        @student = Student.find(params[:id])
    end

    def update #melakukan proses ketika user mengedit data
        @student = Student.find(params[:id])
        @student.update(resource_params)
        flash[:notice] = 'Student has been updated'
        redirect_to student_path(@student)
    end

    def destroy #unuk menghapus data
        @student = Student.find(params[:id])
        @student.destroy
        flash[:notice] = 'Student has been deleted'
        redirect_to students_path
    end

    def index #menampilkan seluruh data yang ada di database
        @students = Student.all
    end

    def show #menampilkan sebuah data secara detail
        id = params[:id]
        @student = Student.find(id)
    end

    private
    def resource_params
        params.require(:student).permit(:name, :username, :age, :kelas, :address, :city, :NIK)
    end

end
