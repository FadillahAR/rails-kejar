class ReportsController < ApplicationController
    def new #untuk menampilkan form data baru
        @report = Report.new
    end

    def create #untuk memproses data baru yang dimasukan di form new
        report = Report.new(resource_params)
        report.save
        flash[:notice] = 'Report has been created'
        redirect_to reports_path
    end

    def edit #menampilkan data yang sudah disimpan di edit
        @report = Report.find(params[:id])
    end

    def update #melakukan proses ketika user mengedit data
        @report = Report.find(params[:id])
        @report.update(resource_params)
        flash[:notice] = 'Report has been updated'
        redirect_to report_path(@report)
    end

    def destroy #unuk menghapus data
        @report = Report.find(params[:id])
        @report.destroy
        flash[:notice] = 'Report has been deleted'
        redirect_to reports_path
    end

    def index #menampilkan seluruh data yang ada di database
        @reports = Report.all
    end

    def show #menampilkan sebuah data secara detail
        id = params[:id]
        @report = Report.find(id)
    end

    private
    def resource_params
        params.require(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id, :date)
    end
end
