class CreatePockets < ActiveRecord::Migration[6.0]
  def up
    create_table :pockets do |t|
      t.string :balance
      t.string :student_id, null:false
      t.string :teacher_id, null:false

      t.timestamps
    end
  end

  def down
    drop_table :pockets
  end
end
