class CreateStudents < ActiveRecord::Migration[6.0]
  def up
    create_table :students do |t|
      t.string :name, limit:40
      t.string :username, limit:15
      t.integer :age 
      t.string :kelas
      t.text :address
      t.string :city
      t.string :NIK, null:false

      t.timestamps
    end
  end

  def down
    drop_table :students
  end
end
