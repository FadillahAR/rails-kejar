class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :title, limit:50
      t.string :mapel, limit:30
      t.string :duration
      t.float :nilai
      t.string :status
      t.string :level
      t.string :student_id, null:false

      t.timestamps
    end
  end

  def down
    drop_table :exams
  end
end
